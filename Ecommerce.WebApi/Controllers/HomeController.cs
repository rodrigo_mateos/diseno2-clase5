﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecommerce.Entities;

namespace Ecommerce.WebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {


            return View();
        }

        public ActionResult Initialize()
        {
            Ecommerce.Entities.Address address = new Address() { Country = "Uruguay", Street = "Cuareim", DoorNumber = 1451, Phone = "29021505", State = "Montevideo", ZipCode = 11000 };

            Ecommerce.Entities.User user1 = new Ecommerce.Entities.User() { Name = "Alejandro", LastName = "Tocar", Email = "aletocar@gmail.com", Address = address, UserId = 1 };
            Ecommerce.Entities.User user2 = new Ecommerce.Entities.User() { Name = "Ignacio", LastName = "Fonseca", Email = "ifonseca@montevideo.com.uy", Address = address, UserId = 2 };
            Ecommerce.Entities.User user3 = new Ecommerce.Entities.User() { Name = "Juan Ignacio", LastName = "Galán", Email = "juanigalan91@gmail.com", Address = address, UserId = 3 };

            Image image = new Image() { ImageLink = "http://www.google.com.uy/imgres?imgurl=http://store.storeimages.cdn-apple.com/4735/as-images.apple.com/is/image/AppleInc/aos/published/images/m/ac/macbook/select/macbook-select-gold-201501%253Fwid%253D1200%2526hei%253D630%2526fmt%253Djpeg%2526qlt%253D95%2526op_sharpen%253D0%2526resMode%253Dbicub%2526op_usm%253D0.5,0.5,0,0%2526iccEmbed%253D0%2526layer%253Dcomp%2526.v%253D1425921710781&imgrefurl=http://www.apple.com/shop/buy-mac/macbook/gold-256gb&h=630&w=1200&tbnid=Y7RTDfJ8wdgCHM:&docid=bxlhjT-Ph0G3WM&ei=hM8EVu-sAYnKetavhIgB&tbm=isch&ved=0CDEQMygBMAFqFQoTCO_-wLuukcgCFQmlHgod1hcBEQ" };

            Product product1 = new Product() { Name = "Taladro", Description = "Permite hacer agujeros de distintos tamaños", Price = 100, Images = new List<Image>() { image }, ProductID = 1 };
            Product product2 = new Product() { Name = "Destornillador", Description = "Permite hacer agujeros de distintos tamaños", Price = 20, Images = new List<Image>() { image }, ProductID = 2 };
            Product product3 = new Product() { Name = "Amoladora", Description = "Permite hacer agujeros de distintos tamaños", Price = 30, Images = new List<Image>() { image }, ProductID = 3 };

            OrderLine ol1 = new OrderLine() { Product = product1, Amount = 10 };
            OrderLine ol2 = new OrderLine() { Product = product2, Amount = 20 };
            OrderLine ol3 = new OrderLine() { Product = product3, Amount = 10 };
            OrderLine ol4 = new OrderLine() { Product = product2, Amount = 2 };

            Order order1 = new Order() { OrderLines = new OrderLine[] { ol1, ol2 }, DateCreated = DateTime.Now.AddDays(-10), DateConfirmed = DateTime.Now.AddDays(-2), Buyer = user1 };
            Order order2 = new Order() { OrderLines = new OrderLine[] { ol3, ol4 }, DateCreated = DateTime.Now.AddDays(-5), DateConfirmed = DateTime.Now, Buyer = user1 };

            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                //Adding users
                ctx.Users.Add(user1);
                ctx.Users.Add(user2);
                ctx.Users.Add(user3);

                //Adding products
                ctx.Products.Add(product1);
                ctx.Products.Add(product2);
                ctx.Products.Add(product3);

                //Adding orders
                ctx.Orders.Add(order1);
                ctx.Orders.Add(order2);

                ctx.SaveChanges();
            }

            return View();
        }
    }
}
